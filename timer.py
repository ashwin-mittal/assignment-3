import pygame
import score


class Time:
    def __init__(self, player, pos):
        self.player = player
        self.start = pygame.time.get_ticks()
        self._score = score.Score(pos, player)

    def show_time(self, screen, font, pos, num):
        timer = (pygame.time.get_ticks() - self.start) / 1000
        time = font.render(
            "Time(" + self.player + "):" +
            str(timer),
            True,
            (0, 0, 0),
        )
        screen.blit(time, (600, 0))
        return self._score.show_score(screen, font, pos, timer, num)
