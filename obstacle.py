class MovObstacle:
    def __init__(self, speed, posX, posY, image):
        self.speed = speed
        self.posX = posX
        self.posY = posY
        self.image = image

    def show(self, screen):
        screen.blit(self.image, (self.posX, self.posY))
        self.posX = self.posX + self.speed
        if self.speed < 0:
            if self.posX <= -64:
                self.posX = 736
        else:
            if self.posX >= 800:
                self.posX = 0


class FixObstacle:
    def __init__(self, posX, posY, image):
        self.posX = posX
        self.posY = posY
        self.image = image

    def show(self, screen):
        screen.blit(self.image, (self.posX, self.posY))
