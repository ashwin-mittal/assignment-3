import pygame


class Configuration:
    def __init__(self):
        # speed of player
        self.player_speed = 3

        # speed of moving obstacles
        self.speed = 3

        # font
        self.font = pygame.font.Font("freesansbold.ttf", 18)
        # color
        self.rfont = 0
        self.gfont = 0
        self.bfont = 0
        # large font
        self.over_font = pygame.font.Font("freesansbold.ttf", 64)
        # color
        self.rlfont = 255
        self.glfont = 255
        self.blfont = 255
        # messages
        self.win = "Success"
        self.lose = "Hit the obstale"
