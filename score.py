class Score:
    def __init__(self, pos, player):
        self.score_value = 0
        self.player = player
        self.pos = pos
        self.max_reach = 32
        self.min_reach = 768
        self.prev = 0
        self.list_a = [718, 618, 568, 468, 418, 318, 268, 168, 118, 18, 0]
        self.list_b = [50, 150, 200, 300, 350, 450, 500, 600, 650, 750, 768]

    def show_score(self, screen, font, pos, time, num):
        if num == 0:
            for i in range(0, 11):
                if self.list_a[i] < pos:
                    break
            if i > self.prev and i % 2 == 0:
                self.score_value += 10
                self.prev = i
            elif i > self.prev:
                self.score_value += 5
                self.prev = i
            self.score = self.score_value - int(time)
        else:
            for i in range(0, 11):
                if self.list_b[i] > pos:
                    break
            if i > self.prev and i % 2 == 0:
                self.score_value += 10
                self.prev = i
            elif i > self.prev:
                self.score_value += 5
                self.prev = i
            self.score = self.score_value - int(time)
        self.scoring = font.render(
            "Score(" + self.player + "):" +
            str(self.score), True, (0, 0, 0))

        screen.blit(self.scoring, (0, 0))
        return self.score
