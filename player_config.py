import timer


class Player:
    def __init__(self, image, player_num, player, posX, posY):
        self.posX = posX
        self.posY = posY
        self.image = image
        self.player_changeX = 0
        self.player_changeY = 0
        self.num = player_num
        self._time = timer.Time(player, posY)

    def show_player(self, screen, font):

        screen.blit(self.image, (self.posX, self.posY))
        return self._time.show_time(screen, font, self.posY, self.num)
