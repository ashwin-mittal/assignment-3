import random

import pygame

from pygame import mixer

import obstacle

import player_config

import configuration

pygame.init()

# background

screen = pygame.display.set_mode((800, 800))

background = pygame.image.load("background.png")

pygame.display.set_caption("River Crossing")

ship = pygame.image.load("ship.png")

pygame.display.set_icon(ship)

# music playing

mixer.music.load("background.mp3")
mixer.music.play(-1)

# player instance

pos = [768, 0]

_set = configuration.Configuration()

playerImg = [pygame.image.load("skull.png"), pygame.image.load("enemy.png")]

num = 0

list = ["Player A", "Player B"]

# winning list

win = []
count = 0
round = 1
winner = ""

# moving obstacles instances

sharkImg = pygame.image.load("shark.png")
boatImg = pygame.image.load("boat.png")
crocImg = pygame.image.load("crocodile.png")
shark2Img = pygame.image.load("shark (2).png")
stoneImg = pygame.image.load("stone.png")
stone2Img = pygame.image.load("stone (2).png")
rockImg = pygame.image.load("rock.png")
rock2Img = pygame.image.load("rocks.png")

_mov = []
for i in range(2):
    _mov.append(obstacle.MovObstacle(
        _set.speed, random.randint(400 * i, 400 + 400 * i), 672, boatImg))

    _mov.append(obstacle.MovObstacle(
        _set.speed, random.randint(400 * i, 400 + 400 * i), 68, sharkImg))

    _mov.append(obstacle.MovObstacle(
        -_set.speed, random.randint(400 * i, 400 + 400 * i), 522, shark2Img
    ))

    _mov.append(obstacle.MovObstacle(
        _set.speed, random.randint(400 * i, 400 + 400 * i), 222, boatImg))

    _mov.append(obstacle.MovObstacle(-_set.speed,
                                     random.randint(200, 800), 372, crocImg))

# fixed obstacles instances

_stone = []
for i in range(0, _set.player_speed):
    _stone.append(obstacle.FixObstacle(
        random.randint(i * 250, 200 + i * 250), 159, stoneImg))

for i in range(_set.player_speed, 6):
    _stone.append(obstacle.FixObstacle(
        random.randint((i - 3) * 250, 200 + (i - 3) * 250), 309, stoneImg))

for i in range(6, 9):
    _stone.append(obstacle.FixObstacle(
        random.randint((i - 6) * 250, 200 + (i - 6) * 250), 459, stone2Img))

for i in range(9, 12):
    _stone.append(obstacle.FixObstacle(
        random.randint((i - 9) * 250, 200 + (i - 9) * 250), 609, stone2Img))

for i in range(12, 14):
    _stone.append(obstacle.FixObstacle(
        random.randint((i - 12) * 500, 300 + (i - 12) * 200), 9, rock2Img))

for i in range(14, 16):
    _stone.append(obstacle.FixObstacle(
        random.randint((i - 14) * 500, 300 + (i - 14) * 200), 759, rockImg))

# collision detection

codis = pow(28, 2)


def Collision():
    for i in range(0, 10):
        dist = pow(_player.posX - _mov[i].posX, 2) + \
            pow(_player.posY - _mov[i].posY, 2)
        if dist <= codis:
            return True
    for i in range(0, 16):
        dist = pow(_player.posX - _stone[i].posX, 2) + \
            pow(_player.posY - _stone[i].posY, 2)
        if dist <= codis:
            return True
    return False

# showing hello screen


def show_hello():
    hello = _set.over_font.render(
        "Hello!", True, (_set.rlfont, _set.glfont, _set.blfont))
    screen.blit(hello, (300, 300))
    hello = _set.over_font.render(
        "Let's Play!", True, (_set.rlfont, _set.glfont, _set.blfont))
    screen.blit(hello, (250, 400))
    pygame.display.update()
    pygame.time.delay(1000)
    global _player
    _player = player_config.Player(
        playerImg[num], num, list[num], 384, pos[num])

# showing player turn


def show_turn():
    message = _set.font.render(
        list[num] + " " + "Turn", True, (_set.rfont, _set.gfont, _set.bfont))
    screen.blit(message, (330, 18))

# showing round


def show_round():
    Round = _set.font.render("Round: " + str(round),
                             True, (_set.rfont, _set.gfont, _set.bfont))
    screen.blit(Round, (350, 0))

# showing winner


def show_winner():
    Win = _set.font.render(winner, True, (_set.rfont, _set.gfont, _set.bfont))
    screen.blit(Win, (290, 36))


def show_success():
    over_text = _set.over_font.render(
        _set.win, True, (_set.rlfont, _set.glfont, _set.blfont))
    screen.blit(over_text, (250, 300))
    pygame.display.update()
    pygame.time.delay(1000)


def show_hit():
    over_text = _set.over_font.render(
        _set.lose, True, (_set.rlfont, _set.glfont, _set.blfont))
    screen.blit(over_text, (200, 300))
    pygame.display.update()
    pygame.time.delay(1000)


start_screen = 0

run = True

while run:

    screen.blit(background, (0, 0))

    if start_screen == 0:
        show_hello()
        start_screen = start_screen + 1

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.KEYDOWN:
            if _player.num == 0:
                if event.key == pygame.K_DOWN:
                    _player.player_changeY = _set.player_speed
                if event.key == pygame.K_UP:
                    _player.player_changeY = -_set.player_speed
                if event.key == pygame.K_RIGHT:
                    _player.player_changeX = _set.player_speed
                if event.key == pygame.K_LEFT:
                    _player.player_changeX = -_set.player_speed

            else:
                if event.key == pygame.K_s:
                    _player.player_changeY = _set.player_speed
                if event.key == pygame.K_w:
                    _player.player_changeY = -_set.player_speed
                if event.key == pygame.K_d:
                    _player.player_changeX = _set.player_speed
                if event.key == pygame.K_a:
                    _player.player_changeX = -_set.player_speed

        if event.type == pygame.KEYUP:
            if _player.num == 0:
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    _player.player_changeY = 0
                if event.key == pygame.K_RIGHT or event.key == pygame.K_LEFT:
                    _player.player_changeX = 0

            else:

                if event.key == pygame.K_w or event.key == pygame.K_s:
                    _player.player_changeY = 0
                if event.key == pygame.K_d or event.key == pygame.K_a:
                    _player.player_changeX = 0

    _player.posY += _player.player_changeY
    _player.posX += _player.player_changeX

    if _player.posY <= 0:
        _player.posY = 0
    elif _player.posY >= 768:
        _player.posY = 768
    if _player.posX <= 0:
        _player.posX = 0
    elif _player.posX >= 768:
        _player.posX = 768

    for i in range(0, 10):
        _mov[i].show(screen)

    for i in range(0, 16):
        _stone[i].show(screen)

    score = _player.show_player(screen, _set.font)

    collision = Collision()

    show_turn()

    show_round()

    show_winner()

    if collision:
        show_hit()
        num = (num + 1) % 2
        _player = player_config.Player(
            playerImg[num], num, list[num], 384, pos[num])
        count = count + 1
        win.append(-1000)
        if count == 2:
            count = 0
            if win[0] > win[1]:
                winner = "Winner(Round: " + str(round) + "): " + list[0]
            elif win[1] > win[0]:
                winner = "Winner(Round: " + str(round) + "): " + list[1]
            else:
                if win[0] == -1000:
                    winner = "Winner(Round: " + str(round) + "): No winner"
                else:
                    winner = "Winner(Round: " + str(round) + "): Tie"
            round = round + 1
            for i in range(0, 10):
                if _mov[i].speed > 0:
                    _mov[i].speed += 0.5
                else:
                    _mov[i].speed -= 0.5
            win = []

    if _player.posY == pos[(num + 1) % 2]:
        show_success()
        num = (num + 1) % 2
        count = count + 1
        win.append(score)
        if count == 2:
            count = 0
            if win[0] > win[1]:
                winner = "Winner(Round: " + str(round) + "): " + list[0]
            elif win[1] > win[0]:
                winner = "Winner(Round: " + str(round) + "): " + list[1]
            else:
                winner = "Winner(Round: " + str(round) + "): Tie"
            round = round + 1
            for i in range(0, 10):
                if _mov[i].speed > 0:
                    _mov[i].speed += 0.5
                else:
                    _mov[i].speed -= 0.5
            win = []
        _player = player_config.Player(
            playerImg[num], num, list[num], 384, pos[num])

    pygame.display.update()
